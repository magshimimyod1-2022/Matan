# client side pink floyd
import socket
import time

HOST = "127.0.0.1"
PORT = 8252
BUFF_LEN = 1024  # buffer length
MAX_OPTION = 8
MIN_OPTION = 1
EXIT = "8"
MENU = """1 Get Albums
2 Get Album Songs
3 Get Song Length
4 Get Song Lyrics
5 Get Song Album
6 Search Song by Name
7 Search Song by Lyrics
8 Quit"""

# function to make the format of the message sent to the server using lambda
msg_struct = lambda func_chosen, input_data: f"ask:{func_chosen}:{input_data}"


def cli_menu():
    """
    prints the menu for the client, gets input function and
    also checks for validation of the input function
    :return: the function the user chose
    """
    func = "0"
    while not (MAX_OPTION >= int(func) >= MIN_OPTION):
        print(MENU)
        func = input("Enter number(1-8): ")

        if not (MAX_OPTION >= int(func) >= MIN_OPTION):
            print("Wrong choice! try again...")
    return func


def disassemble_msg(msg):
    """
    disassembles the incoming message from the user
    :param msg: the message
    :return: the wanted part based off if it is an error or success
    """
    msg = msg.split(":")
    if msg[0] == "error":
        return ": ".join(msg)
    else:
        return msg[1]


def send_and_receive_data(sock, func, data):
    """
    take the function and the data the user inputed and send it to the
    server, then receive the returning message from the server
    :param sock: the socket connection to the server
    :param func: the function the user choose
    :param data: the data the user inputted
    :return: if an error has happend then return false else true
    """
    try:
        sock.sendall(msg_struct(func, data).encode())
        # receive the data from the server
        return_msg = sock.recv(BUFF_LEN).decode()
        return_msg = disassemble_msg(return_msg)

        print("\n", return_msg, "\n")
        return True

    except Exception as e:
        print("error: ", e)
        return False


def main():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        try:
            sock.connect((HOST, PORT))
        except:
            print("server offline")
            input("press enter to exit")

        # get the welcome message
        server_msg = sock.recv(BUFF_LEN).decode()
        print(server_msg)

        func = data = ""
        while func != EXIT:
            func = cli_menu()
            # get data from functions that need it
            if func != "1" and func != EXIT:
                data = input("Enter data: ")
            else:
                data = ""
            # if server disconnects
            if not send_and_receive_data(sock, func, data):
                break

        print("connection aborted\n\n BYE BYE!")
        input("press enter to exit")

if __name__ == "__main__":
    main()
