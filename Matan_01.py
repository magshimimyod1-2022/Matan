import requests as rq


def get_the_100_letter(file_path):
    """
    get the 100 letter from a url of a file
    :param file_path: the url of the file to extract (str)
    :return: the 100 letter
    """
    file_content = rq.get(file_path).text
    return file_content[99]


def find_file_names(response):
    """
    makes a list of all the strings that say 'filexx.nfo' (replace xx with number)
    in a string from a webpage
    :param response: the response of the webpage in a form of text (str)
    :return: the list of file names
    """
    list_of_files = []
    for subStr in response.split('href='):
        file = str(subStr[1:11])  # the place of 'file...'
        if 'file' in file:
            list_of_files.append(file)

    return list_of_files


def extract_password_from_site():
    """
    go through all sub files in a website and get the 100 letter from each to form string
    :return: the final message
    """
    URl = 'http://webisfun.cyber.org.il/nahman/files/'

    response = rq.get(URl).text

    final_message = ''
    for fileName in find_file_names(response):
        final_message += get_the_100_letter(URl + fileName)

    return final_message




def find_most_frequnent(word_list):
    """
    finds the most frequent words in a list using dict
    :param word_list: the list
    :return: sorted dict of the frequency of words
    """
    word_dict = {}
    # put word in dictionary with value of amount of apprirences
    for word in word_list:
        if word not in word_dict:
            word_dict[word] = 1
        else:
            word_dict[word] += 1

    return word_dict
    

def find_max_key(dictionary):
    """
    find the key in a dictionary with the biggest value (number)
    :param dictionary: (dict)
    :return: the key with the max value
    """
    for key, val in dictionary.items():
        if val == max(dictionary.values()):
            return key


def find_most_common_words(words_path, word_amount):
    """
    finds the most common words in a file of words and puts them in a scentence as long as word_amount
    :param words_path: the path to the file containing the words (str)
    :param word_amount: the amount of words to put in the sentence (int)
    :return: the sentence of most frequent words
    """
    with open(words_path, 'r') as word_file:
        word_list = word_file.read().split(' ')  # if words_path is a path to file
    #word_list = rq.get(words_path).text.split(' ')  # if words_path is a link
    frequency_dict = find_most_frequnent(word_list)
    # construct sentence
    sentence = ''
    for i in range(word_amount):
        key = find_max_key(frequency_dict)
        sentence += key + ' '
        frequency_dict.pop(key)
    return sentence[:-1]


def main():
    print("Welcome to the password finder of Nahman!\n\nchoose an option:")
    print("1 - password for first website")
    print("2 - sentence for second website")
    choice = int(input("-> "))

    if choice == 1:
        print("please wait a moment...")
        print('password: ', extract_password_from_site())
    elif choice == 2:
        print('sentence: ', find_most_common_words(r'words.txt', 6))
        # print('sentence: ', find_most_common_words(r'words.txt', 6)) 

if __name__ == '__main__':
    main()

