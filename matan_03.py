#  proxy for film4me
import socket
import datetime
import time

SERVER_HOST = '54.71.128.194'
CLIENT_PORT = 9090
SERVER_PORT = 92
BUFFER_SIZE = 1024
FRANCE_BAN = 'ERROR#"France is banned!"'
BLACKLIST = "blacklist.txt"
MAX_REQUEST = 5


def send_and_rcv_server(msg, port):
    """
    send and receive a message to and from the server
    :param port: the port to send on
    :param msg: the message to send
    :return: the message received
    """
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((SERVER_HOST, port))
        sock.sendall(msg.encode())

        server_msg = sock.recv(BUFFER_SIZE).decode()
        return server_msg


def error_fix(msg):
    # good error:  ERROR#"Invalid year values 1900-200"
    # bad error:  SERVERERROR.0.15175#"End year is less or equal to start year"
    """
    fix the errors that dont show on the ui from the messages
    that come from the server
    :param msg: the returned message from the server
    :return: the fixed message
    """
    if "SERVERERROR" in msg:
        msg = msg.split("#")  # make error regular
        msg[0] = "ERROR"
        msg = "#".join(msg)
    return msg


def fix_jpg(msg):
    """
    fixes message by replacing 'jpg' with '.jpg'
    :param msg: the message
    :return: fixed message
    """
    msg = msg.split("jpg")
    msg = ".jpg".join(msg)
    return msg


def ban_france(msg):
    if "France" in msg:
        msg = FRANCE_BAN
    return msg


def blacklist(msg):
    """
    searches in a blacklist of names if they are in the movie
    settings to ban if it is in it
    :param msg: the movie settings
    :return:
    """
    with open(BLACKLIST, 'r') as bl:
        bl = bl.readlines()
        for name in bl:
            name = name.replace("\n", "")
            if name in msg:
                msg = f'ERROR#"{name}in movie (blacklist)"'
                break
        return msg


def load_balancing(end_time, requests_done, msg):
    """
    a load balancing system makes sure that the user can not
    make more than 5 requests in a single minute, useing datetime
    :param end_time: one minute after starting the program or after load blancing is active
    :param requests_done: the amount of requests made
    :param msg: the message from the server (for making the error)
    :return: the end_time , the amount of requests, the message  (for when they change)
    """

    curr_time = datetime.datetime.now()
    # if the minute is not over and more than 5 requests have been made
    if end_time > curr_time and requests_done >= MAX_REQUEST:
        msg = 'ERROR#"No more request allows! Please wait 1 minute"'
    # if the minute is over, reset values
    elif curr_time > end_time:
        end_time = datetime.datetime.now() + datetime.timedelta(minutes=1)
        requests_done = 0

    return end_time, requests_done, msg


FUNCTION_LIST = [blacklist, ban_france, fix_jpg, error_fix]


def main():
    # create a TCP socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as TCP_sock:
        TCP_sock.bind(('', CLIENT_PORT))
        TCP_sock.listen(1)

        requests_done = 0
        # create time variable with one minute more than current
        end_time = datetime.datetime.now() + datetime.timedelta(minutes=1)
        time.sleep(1)
        print("proxy online")

        while True:
            TCP_cli, cli_add = TCP_sock.accept()  # connect to client
            # get the movie info
            with TCP_cli:
                # get user configurations
                cli_msg = TCP_cli.recv(BUFFER_SIZE).decode()
                print(cli_msg)
                # send to server and get movie
                return_msg = send_and_rcv_server(cli_msg, SERVER_PORT)
                # fix the returned message
                for func in FUNCTION_LIST:
                    return_msg = func(return_msg)
                # load balanceing check
                end_time, requests_done, return_msg = load_balancing(end_time, requests_done, return_msg)
                # send movie info to client
                TCP_cli.sendall(return_msg.encode())
                print(return_msg, "\n")
                requests_done += 1


if __name__ == '__main__':
    main()
time.sleep(10)