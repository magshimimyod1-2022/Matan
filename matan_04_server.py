# server side pink floyd
import socket

HOST = "127.0.0.1"
PORT = 8252
WELCOME = "Welcome to the pink floyd database!\n"
BUFF_LEN = 1024  # buffer length
SUCSES = "success:"
EROR = "error:"
EXIT = "8"


def disassemble_incoming_msg(msg):
    msg = msg.split(":")
    return msg[1], msg[2]


def analize_incoming_data(cli_soc, incoming_msg):
    """
    analies the input data that the client sent to the server,
    it gets the function and func data and sends an apropriate responce
    :param cli_soc: the client socket
    :param incoming_msg: the message the client sent
    :return: the function chosen by the client
    """
    input_func, input_data = disassemble_incoming_msg(incoming_msg)
    send_msg = FUNC_DICT.get(input_func)
    cli_soc.sendall((SUCSES + send_msg).encode())
    return input_func


FUNC_DICT = {"1": "album_list",
             "2": "song_list_in_album",
             "3": "len_of_song",
             "4": "song_words",
             "5": "what_album_is_song",
             "6": "word_in_song_title",
             "7": "word_in_song_lyrix",
             "8": "exit"}
def main():
    # create listening socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as lis_sock:
        lis_sock.bind((HOST, PORT))
        lis_sock.listen(1)
        print("server online, awaiting connection")

        while True:
            cli_soc, cli_add = lis_sock.accept()
            print("client connected")
            input_func = incoming_msg = " "

            with cli_soc:
                cli_soc.sendall(WELCOME.encode())
                # keep stay connected untill client disconnects
                while input_func != EXIT and incoming_msg != "":
                    # receive client request
                    try:
                        incoming_msg = cli_soc.recv(BUFF_LEN).decode()
                        print(incoming_msg)
                    except:  # if client abruptly disconnects
                        break

                    if incoming_msg:
                        input_func = analize_incoming_data(cli_soc, incoming_msg)

            print("client disconnected\n\n")


if __name__ == "__main__":
    main()


